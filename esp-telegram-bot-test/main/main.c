#include "dht22.h"
#include "esp_chip_info.h"
#include "esp_event.h"
#include "esp_flash.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_tls.h"
#include "esp_wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "jsmn.h"
#include "nvs_flash.h"
#include "sdkconfig.h"

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_PSK
#define ESP_WIFI_SAE_MODE WPA3_SAE_PWE_BOTH
#define H2E_IDENTIFIER ""

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1

static const char *TAG = "main";

#define WIFI_SSID "OPTUS_E846A"
#define WIFI_PASS "7MU75XP5Q8"

/*HTTP buffer*/
#define MAX_HTTP_RECV_BUFFER 1024
#define MAX_HTTP_OUTPUT_BUFFER 2048
#define MAX_JSON_SUBVAL_STR_LEN 512
#define MAX_JSON_ELEMENTS 64
#define MAX_JSON_SUBELEMENTS 32
#define MAX_JSON_STR_LEN 512
#define MAX_ID_LENGTH 128
#define MAX_USER_LENGTH 32

/*Telegram configuration*/
#define TOKEN "6784242825:AAHGjwLDJ57-FGQMOrbmE7bCliwnDY2CVbo"
#define BOT_COMMON_URL "https://api.telegram.org/bot"
#define BOT_URL BOT_COMMON_URL TOKEN

extern const char telegram_certificate_pem_start[] asm(
    "_binary_telegram_certificate_pem_start");
extern const char
    telegram_certificate_pem_end[] asm("_binary_telegram_certificate_pem_end");

static bool wifiConnected = false;
static uint64_t _last_received_msg = UINT64_MAX;
static float temperature = 99.9f;
static float humidity = -1.0f;

static void event_handler(void *arg, esp_event_base_t event_base,
                          int32_t event_id, void *event_data) {
  if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
    esp_wifi_connect();
  } else if (event_base == WIFI_EVENT &&
             event_id == WIFI_EVENT_STA_DISCONNECTED) {
    wifiConnected = false;
    ESP_LOGI(TAG, "connect to the AP fail, retry to connect to the AP");
    esp_wifi_connect();
  } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
    ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
    ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
    wifiConnected = true;
  }
}

void wifi_init_sta(void) {
  ESP_ERROR_CHECK(esp_netif_init());

  ESP_ERROR_CHECK(esp_event_loop_create_default());
  esp_netif_create_default_wifi_sta();

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  esp_event_handler_instance_t instance_any_id;
  esp_event_handler_instance_t instance_got_ip;
  ESP_ERROR_CHECK(esp_event_handler_instance_register(
      WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL, &instance_any_id));
  ESP_ERROR_CHECK(esp_event_handler_instance_register(
      IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL, &instance_got_ip));

  wifi_config_t wifi_config = {
      .sta =
          {
              .ssid = WIFI_SSID,
              .password = WIFI_PASS,
              .sae_pwe_h2e = ESP_WIFI_SAE_MODE,
              .sae_h2e_identifier = H2E_IDENTIFIER,
          },
  };
  wifi_config.sta.threshold.authmode = ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD;

  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
  ESP_ERROR_CHECK(esp_wifi_start());

  ESP_LOGI(TAG, "wifi_init_sta finished");
}

esp_err_t _http_event_handler(esp_http_client_event_t *evt) {
  static char *output_buffer; // Buffer to store response of http request from
                              // event handler
  static int output_len;      // Stores number of bytes read
  switch (evt->event_id) {
  case HTTP_EVENT_ERROR:
    ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
    break;
  case HTTP_EVENT_ON_CONNECTED:
    ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
    break;
  case HTTP_EVENT_HEADER_SENT:
    ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
    break;
  case HTTP_EVENT_ON_HEADER:
    ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key,
             evt->header_value);
    break;
  case HTTP_EVENT_ON_DATA:
    ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
    // Clean the buffer in case of a new request
    if (output_len == 0 && evt->user_data) {
      // we are just starting to copy the output data into the use
      memset(evt->user_data, 0, MAX_HTTP_OUTPUT_BUFFER);
    }
    /*
     *  Check for chunked encoding is added as the URL for chunked encoding used
     * in this example returns binary data. However, event handler can also be
     * used in case chunked encoding is used.
     */
    if (!esp_http_client_is_chunked_response(evt->client)) {
      // If user_data buffer is configured, copy the response into the buffer
      int copy_len = 0;
      if (evt->user_data) {
        // The last byte in evt->user_data is kept for the NULL character in
        // case of out-of-bound access.
        copy_len = MIN(evt->data_len, (MAX_HTTP_OUTPUT_BUFFER - output_len));
        if (copy_len) {
          memcpy(evt->user_data + output_len, evt->data, copy_len);
        }
      } else {
        int content_len = esp_http_client_get_content_length(evt->client);
        if (output_buffer == NULL) {
          // We initialize output_buffer with 0 because it is used by strlen()
          // and similar functions therefore should be null terminated.
          output_buffer = (char *)calloc(content_len + 1, sizeof(char));
          output_len = 0;
          if (output_buffer == NULL) {
            ESP_LOGE(TAG, "Failed to allocate memory for output buffer");
            return ESP_FAIL;
          }
        }
        copy_len = MIN(evt->data_len, (content_len - output_len));
        if (copy_len) {
          memcpy(output_buffer + output_len, evt->data, copy_len);
        }
      }
      output_len += copy_len;
    }

    break;
  case HTTP_EVENT_ON_FINISH:
    ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
    if (output_buffer != NULL) {
      // Response is accumulated in output_buffer. Uncomment the below line to
      // print the accumulated response ESP_LOG_BUFFER_HEX(TAG, output_buffer,
      // output_len);
      free(output_buffer);
      output_buffer = NULL;
    }
    output_len = 0;
    break;
  case HTTP_EVENT_DISCONNECTED:
    ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
    int mbedtls_err = 0;
    esp_err_t err = esp_tls_get_and_clear_last_error(
        (esp_tls_error_handle_t)evt->data, &mbedtls_err, NULL);
    if (err != 0) {
      ESP_LOGI(TAG, "Last esp error code: 0x%x", err);
      ESP_LOGI(TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
    }
    if (output_buffer != NULL) {
      free(output_buffer);
      output_buffer = NULL;
    }
    output_len = 0;
    break;
  case HTTP_EVENT_REDIRECT:
    ESP_LOGD(TAG, "HTTP_EVENT_REDIRECT");
    esp_http_client_set_header(evt->client, "From", "user@example.com");
    esp_http_client_set_header(evt->client, "Accept", "text/html");
    esp_http_client_set_redirection(evt->client);
    break;
  }
  return ESP_OK;
}

static void cstr_rm_char(char *str, const size_t str_len, const char c_remove) {
  size_t a, b;

  a = 0;
  for (b = 0; b < str_len; b++) {
    if (str[b] != c_remove) {
      str[a] = str[b];
      a = a + 1;
    }
  }
  str[a] = '\0';
}

static uint32_t json_parse_str(const char *json_str, const size_t json_str_len,
                               jsmntok_t *json_tokens,
                               const uint32_t json_tokens_len) {
  jsmn_parser json_parser;
  int num_elements;

  jsmn_init(&json_parser);
  num_elements = jsmn_parse(&json_parser, json_str, json_str_len, json_tokens,
                            json_tokens_len);
  if (num_elements < 0) {
    ESP_LOGE("JSON", "Can't parse JSON data. Code %d\n", num_elements);
    return 0;
  }
  if ((num_elements == 0) || (json_tokens[0].type != JSMN_OBJECT)) {
    ESP_LOGE("JSON", "Can't parse JSON data (invalid sintax?).");
    return 0;
  }

  return num_elements;
}

// Check if given json object contains the provided key
static uint32_t json_has_key(const char *json_str, jsmntok_t *json_tokens,
                             const uint32_t num_tokens, const char *key) {
  for (uint32_t i = 0; i < num_tokens; i++) {
    // Continue to next iteration if json element is not a string
    if (json_tokens[i].type != JSMN_STRING)
      continue;

    // Continue to next iteration if key and json elements lengths are different
    if (strlen(key) !=
        (unsigned int)(json_tokens[i].end - json_tokens[i].start))
      continue;

    // Check if key and json element string are the same
    if (strncmp(json_str + json_tokens[i].start, key,
                json_tokens[i].end - json_tokens[i].start) == 0)
      return i;
  }
  return 0;
}

// Get the corresponding string of given json element (token)
static void json_get_element_string(const char *json_str, jsmntok_t *token,
                                    char *converted_str,
                                    const uint32_t converted_str_len) {
  const char *value = json_str + token->start;
  uint32_t value_len = token->end - token->start;

  if (value_len > converted_str_len)
    value_len = converted_str_len;

  memset(converted_str, '\0', converted_str_len);
  memcpy(converted_str, value, value_len);
  // for(uint32_t i = 0; i < value_len; i++) // Dont trust memcpy, however it is
  // faster...
  //{
  //     converted_str[i] = value[i];
  //     _yield();
  // }
}

static void getMe_perform(void) {
  char receiveBuffer[MAX_HTTP_OUTPUT_BUFFER] = {0};
  esp_http_client_config_t config = {
      .url = "https://api.telegram.org",
      .transport_type = HTTP_TRANSPORT_OVER_SSL,
      .event_handler = _http_event_handler,
      .cert_pem = telegram_certificate_pem_start,
      .user_data =
          receiveBuffer, // Pass address of local buffer to get response
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  /* Creating the string of the url*/
  char url[512] = BOT_URL;
  // Adding the method
  strcat(url, "/getMe");
  ESP_LOGI("GET_ME", "GetMe url: %s", url);
  // You set the real url for the request
  esp_http_client_set_url(client, url);
  esp_http_client_set_method(client, HTTP_METHOD_GET);
  esp_err_t err = esp_http_client_perform(client);
  if (err == ESP_OK) {
    ESP_LOGI("GET_ME", "getMe Perform output: %s", receiveBuffer);
  } else {
    ESP_LOGE("GET_ME", "Error perform http request %s", esp_err_to_name(err));
  }

  esp_http_client_close(client);
  esp_http_client_cleanup(client);
}

static void sendMessage(const char *chatId, const char *message) {
  char request[256] = {0};
  char output_buffer[MAX_HTTP_OUTPUT_BUFFER] = {0};
  snprintf(request, sizeof(request), "{\"chat_id\":%s,\"text\":\"%s\"}", chatId,
           message);

  esp_http_client_config_t config = {
      .url = "https://api.telegram.org",
      .transport_type = HTTP_TRANSPORT_OVER_SSL,
      .event_handler = _http_event_handler,
      .cert_pem = telegram_certificate_pem_start,
      .user_data = output_buffer,
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);

  char url[512] = BOT_URL;
  // Adding the method
  strcat(url, "/sendMessage");
  ESP_LOGI("SEND MESSAGE", "sendMessage url: %s", url);
  esp_http_client_set_url(client, url);

  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_header(client, "Content-Type", "application/json");
  esp_http_client_set_post_field(client, request, strlen(request));

  esp_err_t err = esp_http_client_perform(client);
  if (err == ESP_OK) {
    ESP_LOGI(TAG, "sendMessage POST Status = %d, content_length = %d",
             (int)esp_http_client_get_status_code(client),
             (int)esp_http_client_get_content_length(client));
    ESP_LOGI(TAG, "sendMessage result: %s", output_buffer);

  } else {
    ESP_LOGE(TAG, "sendMessage POST request failed: %s", esp_err_to_name(err));
  }

  esp_http_client_close(client);
  esp_http_client_cleanup(client);
}

static void getUpdates_perform(void) {
  char request[512] = {};
  snprintf(request, sizeof(request),
           "{\"offset\":%llu,\"allowed_updates\":[\"message\"]}",
           _last_received_msg);
  ESP_LOGI("GET UPDATES", "Trying to send getUpdates request (%s)...", request);

  char receiveBuffer[MAX_HTTP_OUTPUT_BUFFER] = {0};
  jsmntok_t _json_elements[MAX_JSON_ELEMENTS];
  jsmntok_t _json_subelements[MAX_JSON_SUBELEMENTS];
  char _json_value_str[MAX_JSON_STR_LEN];
  char _json_subvalue_str[128];
  char recvText[128] = {0};
  char fromId[MAX_ID_LENGTH] = {0};

  esp_http_client_config_t config = {
      .url = "https://api.telegram.org",
      .transport_type = HTTP_TRANSPORT_OVER_SSL,
      .event_handler = _http_event_handler,
      .cert_pem = telegram_certificate_pem_start,
      .user_data =
          receiveBuffer, // Pass address of local buffer to get response
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);
  char url[512] = BOT_URL;
  // Adding the method
  strcat(url, "/getUpdates");

  ESP_LOGI("GET_UPDATES", "getUpdates url: %s", url);

  esp_http_client_set_url(client, url);
  esp_http_client_set_method(client, HTTP_METHOD_POST);
  esp_http_client_set_header(client, "Content-Type", "application/json");
  esp_http_client_set_post_field(client, request, strlen(request));

  esp_err_t err = esp_http_client_perform(client);
  if (err == ESP_OK) {
    ESP_LOGI("GET_UPDATES", "HTTP POST Status = %d, content_length = %d",
             (int)esp_http_client_get_status_code(client),
             (int)esp_http_client_get_content_length(client));

  } else {
    ESP_LOGE("GET_UPDATES", "HTTP POST request failed: %s",
             esp_err_to_name(err));
  }

  esp_http_client_close(client);
  esp_http_client_cleanup(client);

  if (err == ESP_OK) {
    // Use a pointer to received buffer data
    char *ptr_response = receiveBuffer;

    // Remove any EOL character
    cstr_rm_char(ptr_response, strlen(ptr_response), '\r');
    cstr_rm_char(ptr_response, strlen(ptr_response), '\n');

    // Remove start and end list characters ('[' and ']') from response and just
    // keep json structure
    if (strlen(ptr_response) >= 2) {
      if (ptr_response[strlen(ptr_response) - 1] == ']')
        ptr_response[strlen(ptr_response) - 1] = '\0';
      if (ptr_response[0] == '[') {
        ptr_response[0] = '\0';
        ptr_response = ptr_response + 1;
      }
    }

    if (ptr_response[0] == '\0') {
      ESP_LOGI("GET_UPDATES", "getUpdate no new messages");
      return;
    }

    ESP_LOGI("GET_UPDATES", "Response received: %s", ptr_response);

    uint32_t num_subelements;
    uint32_t key_position;

    // Clear json elements objects
    memset(_json_elements, 0, (sizeof(jsmntok_t) * MAX_JSON_ELEMENTS));
    memset(_json_subelements, 0, (sizeof(jsmntok_t) * MAX_JSON_SUBELEMENTS));

    // Parse message string as JSON and get each element
    uint32_t num_elements = json_parse_str(ptr_response, strlen(ptr_response),
                                           _json_elements, MAX_JSON_ELEMENTS);
    if (num_elements == 0) {
      ESP_LOGE("GET_UPDATES", "Bad JSON syntax from received response");

      // Ignore this message that can't be readed and increase counter to ask
      // for the next one
      // TODO: check
      // _last_received_msg = _last_received_msg + 1;

      return;
    }

    // Check and get value of key: update_id
    key_position =
        json_has_key(ptr_response, _json_elements, num_elements, "update_id");
    if (key_position != 0) {
      // Get json element string
      json_get_element_string(ptr_response, &_json_elements[key_position + 1],
                              _json_value_str, MAX_JSON_STR_LEN);

      // Save value in variable
      sscanf(_json_value_str, "%" SCNu64, &_last_received_msg);

      // Prepare variable to next update message request (offset)
      _last_received_msg = _last_received_msg + 1;
      ESP_LOGI(TAG, "New last msg id: %llu", _last_received_msg);
    }

    // Check and get value of key: text
    key_position =
        json_has_key(ptr_response, _json_elements, num_elements, "text");
    if (key_position != 0) {
      // Get json element string
      json_get_element_string(ptr_response, &_json_elements[key_position + 1],
                              _json_subvalue_str, 128);

      // Save value in variable
      snprintf(recvText, sizeof(recvText), "%s", _json_subvalue_str);
    }

    // Check and get value of key: from
    key_position =
        json_has_key(ptr_response, _json_elements, num_elements, "from");
    if (key_position != 0) {
      // Get json element string
      json_get_element_string(ptr_response, &_json_elements[key_position + 1],
                              _json_value_str, MAX_JSON_STR_LEN);
      ESP_LOGW(TAG, "from content: %s", _json_value_str);

      // Parse string "from" content as JSON and get each element
      num_subelements = json_parse_str(_json_value_str, strlen(_json_value_str),
                                       _json_subelements, MAX_JSON_SUBELEMENTS);
      if (num_subelements == 0) {
        ESP_LOGE(TAG, "[Bot] Error: Bad JSON sintax in \"from\" element.");
        return;
      } else {

        // Check and get value of key: id
        key_position = json_has_key(_json_value_str, _json_subelements,
                                    num_subelements, "id");
        ESP_LOGE(TAG, "Num subelements: %lu", num_subelements);
        ESP_LOGE(TAG, "Key position: %lu", key_position);

        if (key_position != 0) {
          // Get json element string
          json_get_element_string(_json_value_str,
                                  &_json_subelements[key_position + 1],
                                  _json_subvalue_str, 128);
          // Save value in variable
          snprintf(fromId, MAX_ID_LENGTH, "%s", _json_subvalue_str);
        }
      }
    }

    ESP_LOGI(TAG, "Text: '%s' from: '%s'", recvText, fromId);

    if (strlen(recvText) && strlen(fromId)) {

      if (strcmp(recvText, "/get_temp") == 0) {
        ESP_LOGI(TAG, "Sending Temperature...");
        char temMessage[64] = {0};
        snprintf(temMessage, sizeof(temMessage), "Current temperature: %.1f, humidity: %.1f%%", temperature, humidity);
        sendMessage(fromId, temMessage);
      } else {
        sendMessage(fromId, "Напиши /get_temp чтобы узнать температуру в доме");
      }
    }
  }
}

static void telegramBotTask(void *pvParameters) {
  ESP_LOGI(TAG, "Telegram bot URL: %s", BOT_URL);
  while (true) {
    if (wifiConnected) {
      getUpdates_perform();
    }
    vTaskDelay(pdMS_TO_TICKS(1000));
  }
}

static void dhtTask(void *pvParameters) {
  ESP_LOGI("DHT", "Starting task...");

  while (1) {

    ESP_LOGI("DHT", "Reading...");
    int ret = readDHT();

    errorHandler(ret);

    temperature = getTemperature();
    humidity = getHumidity();
    ESP_LOGI("DHT", "Hum %.1f", humidity);
    ESP_LOGI("DHT", "Tmp %.1f", temperature);

    // -- wait at least 2 sec before reading again ------------
    // The interval of whole process must be beyond 2 seconds !!
    vTaskDelay(pdMS_TO_TICKS(3000));
  }
}

void app_main(void) {
  printf("Hello world!\n");

  /* Print chip information */
  esp_chip_info_t chip_info;
  uint32_t flash_size;
  esp_chip_info(&chip_info);
  printf("This is %s chip with %d CPU core(s), %s%s%s%s, ", CONFIG_IDF_TARGET,
         chip_info.cores,
         (chip_info.features & CHIP_FEATURE_WIFI_BGN) ? "WiFi/" : "",
         (chip_info.features & CHIP_FEATURE_BT) ? "BT" : "",
         (chip_info.features & CHIP_FEATURE_BLE) ? "BLE" : "",
         (chip_info.features & CHIP_FEATURE_IEEE802154)
             ? ", 802.15.4 (Zigbee/Thread)"
             : "");

  unsigned major_rev = chip_info.revision / 100;
  unsigned minor_rev = chip_info.revision % 100;
  printf("silicon revision v%d.%d, ", major_rev, minor_rev);
  if (esp_flash_get_size(NULL, &flash_size) != ESP_OK) {
    printf("Get flash size failed");
    return;
  }

  printf("%" PRIu32 "MB %s flash\n", flash_size / (uint32_t)(1024 * 1024),
         (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded"
                                                       : "external");

  printf("Minimum free heap size: %" PRIu32 " bytes\n",
         esp_get_minimum_free_heap_size());

  // Initialize NVS
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
      ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  wifi_init_sta();
  xTaskCreate(&telegramBotTask, "telegramBotTask", 8192 * 4, NULL, 5, NULL);
  xTaskCreate(&dhtTask, "dhtTask", 2048, NULL, 5, NULL);
  vTaskDelete(NULL);
}